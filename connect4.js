// Group G
// Marie Friis
// Hector Taveraz
// Andrew Belanger
// * Justin Miller


let currentPlayer = "red"; // "red" "black"
const gameBoard = document.getElementById("gameboard");
let condition = "";
console.log(condition);

// "red" "black" null
let board = [
    //columns
    [null, null, null, null, null, null, null],//rows
    [null, null, null, null, null, null, null],
    [null, null, null, null, null, null, null],
    [null, null, null, null, null, null, null],
    [null, null, null, null, null, null, null],
    [null, null, null, null, null, null, null]
]




function addDiskToBoard(color, boardToUpdate, selectedColumn) {
    let updatedBoard = boardToUpdate
    let selectedRow
    let rowToUpdate
    for (let i = (boardToUpdate.length - 1); i >= 0; i--) {
        rowToUpdate = boardToUpdate[i]
        if (rowToUpdate[selectedColumn] === null) {
            selectedRow = i
            break
        }
    }
    rowToUpdate[selectedColumn] = color
    updatedBoard[selectedRow]
    return updatedBoard
}

function displayBoardInHTML(boardToDisplay) {
    for (rowSel=5; rowSel >= 0; rowSel--){
        let currentRow = boardToDisplay[rowSel];
        for (colCell = 0; colCell < 7; colCell++){
            cellInHTML = document.getElementById(rowSel + "-" + colCell);
            cellInHTML.dataset.color = currentRow[colCell];
        }
    }
}

function checkRowWin(boardToCheck){
    for(let j = 0; j <= 5; j++){
        let blackCount = 0;
        let redCount = 0;
        for(let i = 0; i <= 6; i++){
            if (board[j][i] === null){continue}
            else if (board[j][i] === "black"){
                blackCount++;
                redCount = 0;
                if(blackCount === 4){console.log("black wins horizontal"); condition= "black";}
            }
            else if (board[j][i] === "red"){
                redCount++;
                blackCount = 0;
                if(redCount === 4){console.log("red wins horizontal"); condition= "red";}
            }
        }
    }
    return condition;
}

function checkColWin(boardToCheck) {
    for(let j = 0; j <= 6; j++){
        let blackCount = 0;
        let redCount = 0;
        for(let i = 0; i <= 5; i++){
            if (board[i][j] === null){continue}
            else if (board[i][j] === "black"){
                blackCount++;
                redCount = 0;
                if(blackCount === 4){console.log("black win vertically"); condition= "black"}
            }
            else if (board[i][j] === "red"){
                redCount++;
                blackCount = 0;
                if(redCount === 4){console.log("red win vertically"); condition= "red"}
            }
        }
    }
    return condition
}

function checkDiagonalWin(boardToCheck){

let boardArray = [];
for(let i=0; i<=5; i++){
    boardPart=boardToCheck[i];
        for(let p=0; p<boardPart.length; p++){
            boardArray.push(boardPart[p]);
        }
}

function winningSlice(arrSlice){
    let redWinCt = 0;
    let blackWinCt = 0;
    for(w=0; w<=3; w++){
        if (arrSlice[w] === "red"){redWinCt++;}
        if (arrSlice[w] === "black"){blackWinCt++;}
        if (redWinCt === 4){console.log("Red win Diagonal"); condition= "red";}
        if (blackWinCt === 4){console.log("Black win Diagonal"); condition= "black";}
    }
}

for(let i=0; i<=3; i++){
    let arrSlice = [];
    arrSlice.push(boardArray[i]);
    arrSlice.push(boardArray[i+8]);
    arrSlice.push(boardArray[i+16]);
    arrSlice.push(boardArray[i+24]);
    winningSlice(arrSlice);
    
}
for(let i=1; i<=4; i++){
    let arrSlice = [];
    arrSlice.push(boardArray[i+6]);
    arrSlice.push(boardArray[i+14]);
    arrSlice.push(boardArray[i+22]);
    arrSlice.push(boardArray[i+30]);
    winningSlice(arrSlice);
}
for(let i=1; i<=4; i++){
    let arrSlice = [];
    arrSlice.push(boardArray[i+13]);
    arrSlice.push(boardArray[i+21]);
    arrSlice.push(boardArray[i+29]);
    arrSlice.push(boardArray[i+37]);
    winningSlice(arrSlice);
}

for(let i=1; i<=4; i++){
    let arrSlice = [];
    arrSlice.push(boardArray[34+i]);
    arrSlice.push(boardArray[28+i]);
    arrSlice.push(boardArray[22+i]);
    arrSlice.push(boardArray[16+i]);
    winningSlice(arrSlice);
}

for(let i=1; i<=4; i++){
    let arrSlice = [];
    arrSlice.push(boardArray[27+i]);
    arrSlice.push(boardArray[21+i]);
    arrSlice.push(boardArray[15+i]);
    arrSlice.push(boardArray[9+i]);
    winningSlice(arrSlice);
}

for(let i=1; i<=4; i++){
    let arrSlice = [];
    arrSlice.push(boardArray[20+i]);
    arrSlice.push(boardArray[14+i]);
    arrSlice.push(boardArray[8+i]);
    arrSlice.push(boardArray[2+i]);
    winningSlice(arrSlice);
}
return condition;
}



function checkforTie(boardToCheck){
let boardArray = [];
let takenCells = 0;

for(let i=0; i<=5; i++){
    boardPart=boardToCheck[i];
        for(let p=0; p<boardPart.length; p++){
            boardArray.push(boardPart[p]);
        }
}

for(let i=0; i<boardArray.length; i++){
    if(boardArray[i] != null){takenCells++}
}

if(takenCells === 42){console.log("its a tie"); condition= "It is a Tie!"}

return condition;
}



function checkForEndingCondition(boardToCheck) {
    // ending conditions: "red win", "black win", "tie", "" (keep playing)
    checkRowWin(boardToCheck);
    checkColWin(boardToCheck);
    checkDiagonalWin(boardToCheck);
    checkforTie(boardToCheck);
    return condition;
}

function showMessage(condition) {
    // Tell the user if someone has won or there is a tie
    if(condition === "red"){alert("Red Wins!");}
    if(condition === "black"){alert("Black Wins!");}
    if(condition === "tie"){alert("It is a Tie!");}
}

function togglePlayer(color) {
    if (currentPlayer === "red"){currentPlayer = "black"} 
    else if (currentPlayer === "black"){currentPlayer = "red"}
    return currentPlayer;
}

gameBoard.addEventListener('click', function (evt) {
    let clickedColumn = evt.target.dataset.col;
    if (clickedColumn === undefined) { return };
    board = addDiskToBoard(currentPlayer, board, clickedColumn);
    displayBoardInHTML(board);
    checkForEndingCondition(board);
    // "red win", "black win", "tie", ""
    if (condition !== "") { showMessage(condition) }
    else { currentPlayer = togglePlayer(currentPlayer) }
    turnSpan.innerHTML=currentPlayer;
 })